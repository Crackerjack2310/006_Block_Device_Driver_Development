#include"header.h"
#include"declarations.h"
unsigned int majorno, nsectors, hardsect_size;
Dev *device;			/* Our Block Device */
static int __init init_func(void)
{
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : Hello Kernel !!!\n", __func__);
	#endif 
	/*
	 * Get a major number for the driver
	 * Set device params from the datasheet
	 */
	majorno = MAJORNO;
	nsectors = NSECTORS;
	hardsect_size = HARDSECT_SIZE;			
	/*
	 * Register block device
	 * get a dynamically allocated majorno
	 * create an entry in /proc/devices
	 */ 
	majorno = register_blkdev(majorno, DEVNAME);
	if (majorno < 0)
	{
		#ifdef DEBUG
		printk(KERN_INFO "register_blkdev() failed with error code : %d\n", majorno);	
		#endif 
		goto OUT;
	} 	
	#ifdef DEBUG
	printk(KERN_INFO "%s : Got Major no : %d\n", __func__, majorno);	
	#endif
	/* 
	 * Make space for your Device 
	 * Clear the device structure
	 */
	device = (Dev*)kmalloc(sizeof(Dev), GFP_KERNEL);
	if (!device)
	{	
		#ifdef DEBUG 
		printk(KERN_ERR "%s : Unable to allocate memory for device\n", __func__);
		#endif
		goto OUT;
	}
	memset(device, '\0', sizeof(Dev)); 
	/*
	 * calculate memory required for buffer 
	 * allocate that much amount using vmalloc()
	 * clear the buffer structure
	 */
	device->size = nsectors * hardsect_size;
	device->data = vmalloc(device->size);		// Why ???
	if (!device->data)
	{
		#ifdef DEBUG 
		printk(KERN_ERR "%s : Unable to allocate memory for buffer\n", __func__);
		#endif
		goto OUT;
	}
	memset(device->data, '\0', sizeof(device->size));
	#ifdef DEBUG
	printk(KERN_INFO "%s : Successfully allocated buffer of %d bytes\n", __func__, device->size);	
	#endif     			  
	/* 
	 * Get that Spinlock. 
	 */
	spin_lock_init(&device->lock);
	/*
	 * Initialize the request queue, get a pointer to the first node of the request queue linked list
	 */
	device->queue = blk_init_queue(dev_req_func, &device->lock);
	if (!device->queue)
	{
		#ifdef DEBUG
		printk(KERN_ERR "%s : Unable to initialize block device init queue\n", __func__);
		#endif     			  
		/*
		 * de-allocate the memory in case of an unexpected exit
		 */
		vfree(device->data);
		device->data = NULL;
		#ifdef DEBUG
		printk(KERN_ERR "%s : Freed up the device->data buffer\n", __func__);
		#endif     			  
		kfree(device);
		device = NULL;
		#ifdef DEBUG
		printk(KERN_ERR "%s : Freed up the device buffer\n", __func__);
		#endif     			  
		goto OUT;
	}
	/* 
	 * Set logical block size
	 */
	#ifdef DEBUG
	printk(KERN_INFO "%s : Successfully Initialized block device queue\n", __func__);
	#endif     			  
	/*  
	 * Next, set a logical block size for the device
	 * Time to preserve our request in a self referencial way
	 * queuedata is of type void *
	 * queue is the address of the first node of the request_queue linked list
	 */        
	blk_queue_logical_block_size(device->queue, hardsect_size);
	device->queue->queuedata = device; 
	#ifdef DEBUG  
	printk(KERN_INFO "%s : End\n", __func__);
	#endif 
	return 0;
OUT:	
	#ifdef DEBUG
	printk("Block device registeration failed due to some errors !!!\n");
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : End", __func__);
	#endif
	return 1;
}
module_init(init_func);

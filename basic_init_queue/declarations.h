#ifndef DEVNAME
#define DEVNAME "my_block_device"
#endif
#ifndef DEBUG
#define DEBUG
#endif
#ifndef MAJORNO
#define MAJORNO 0
#endif 
#ifndef NSECTORS
#define NSECTORS  10
#endif
#ifndef HARDSECT_SIZE
#define HARDSECT_SIZE	512
#endif  
void dev_req_func( struct request_queue * );
extern unsigned int majorno, nsectors, hardsect_size;
typedef struct Dev
{
	int size;			// total size in terms of sectors (nsectors * hardsect_size)
	u8 *data;			// data buffer
	short users;			//  
	short media_change;		// flag for detecting change in media
	spinlock_t lock;		// spinlock for mutual exclusion	
	struct request_queue *queue;    // start/head node of request_queue
	struct gendisk *gd;		// disck structure for exch partition 
	struct timer_list timer;	// timers to use
}Dev;
/*typedef struct request
{
	struct request_queue *q;	// container request queue
	sector_t sector;		// the sector from which data is requested
	unsigned long nr_sectors;	// no of sectors left to submit
	struct bio *bio;		// associated bio	
	char *buffer;			// this is the same as Dev->data
	struct request *next_rq;	// pointer to next node in the request queue
}request;*/						
extern Dev *device;				

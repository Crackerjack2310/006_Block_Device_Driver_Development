#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/blkdev.h>
#include<linux/fs.h>
#include<linux/vmalloc.h>
#include<linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sankalp Negi <sankalpnegi2310@gmail.com>");
MODULE_DESCRIPTION("Block Device Driver");

#include"header.h"
#include"declarations.h"
static void cleanup_func(void)
{
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "Bye kernel...See u again !!!\n");
	#endif
	vfree(device->data);
	unregister_blkdev(majorno, DEVNAME);
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
}
module_exit(cleanup_func);

#ifndef DEVNAME
#define DEVNAME "My_Device"
#endif

#ifndef DEBUG
#define DEBUG
#endif

#ifndef MAJORNO
#define MAJORNO 0
#endif

#ifndef NSECTORS
#define NSECTORS  10
#endif

#ifndef HARDSECT_SIZE
#define HARDSECT_SIZE	512
#endif

extern unsigned int majorno, nsectors, hardsect_size;
typedef struct Dev
{
	int size;
	u8 *data;
	short users;
	short media_change;
	spinlock_t lock;
	struct request_queue *queue;
	struct gendsk *gd;
	struct timer_list timer;
}Dev;
extern Dev *device;

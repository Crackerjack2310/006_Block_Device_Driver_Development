#include"header.h"
#include"declarations.h"
#include"fops.h"			/* for file_operations data structure */
unsigned int majorno, nsectors, hardsect_size;
Dev *device;
static int __init init_func(void)
{
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "Hello Kernel !!!\n");
	#endif
	majorno = MAJORNO;
	nsectors = NSECTORS;
	hardsect_size = HARDSECT_SIZE;			
	majorno = register_blkdev(majorno, DEVNAME);	//returns majorno directly
	if (!majorno)
		goto OUT;
	#ifdef DEBUG
	printk(KERN_INFO "Major no : %d\n", majorno);	
	#endif
	/* Make space for Device */
	device = (Dev*)kmalloc(sizeof(Dev), GFP_KERNEL);
	if (!device)
	{
		printk(KERN_ERR "%s : Unable to allocate memory for device\n", __func__);
		goto OUT;
	}
	/* Set device params */
	device->size = nsectors * hardsect_size;
	#ifdef DEBUG
	printk(KERN_INFO "Got device size as : %d sectors\n", device->size);	
	#endif
	device->data = vmalloc(device->size);
	if (!device->data)
	{
		printk(KERN_ERR "%s : Unable to allocate memory for device->size\n", __func__);
		goto OUT;
	}
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
	return 0;
OUT:	
	#ifdef DEBUG
	printk("register_blkdev failed !!! with  : %d\n", majorno);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : End", __func__);
	#endif
	return 1;
}
module_init(init_func);

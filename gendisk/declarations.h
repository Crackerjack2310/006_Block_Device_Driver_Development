#ifndef DECLARATIONS_H
#define	DECLARATIONS_H
#ifndef DEVNAME
#define DEVNAME "sbull" 		/* This is what shows up in /proc and /sysfs */
#endif
#ifndef DEBUG				/* Remove this to disable logging */
#define DEBUG
#endif
#ifndef MAJORNO
#define MAJORNO 0
#endif 
#ifndef MINORNO
#define MINORNO 0
#endif 
#ifndef PARTITIONS
#define PARTITIONS 1
#endif 	
#ifndef NSECTORS
#define NSECTORS 100
#endif
#ifndef HARDSECT_SIZE
#define HARDSECT_SIZE 512
#endif
#ifndef KERNEL_SECTOR_SIZE
#define KERNEL_SECTOR_SIZE 512	
#endif			
typedef struct Dev {
	int size;			// total size in terms of sectors (nsectors * hardsect_size)
	u8 *data;			// data buffer
	short users;			// for users 
	short media_change;		// flag for detecting change in media
	spinlock_t lock;		// spinlock for mutual exclusion	
	struct request_queue *queue;    // start/head node of request_queue
	struct gendisk *gd;		// disk structure for each partition 
	struct timer_list timer;	// timers to use
}Dev;
/*typedef struct request {
	struct request_queue *q;	// container request queue
	sector_t sector;		// the sector from which data is requested
	unsigned long nr_sectors;	// no of sectors left to submit
	struct bio *bio;		// associated bio	
	char *buffer;			// this is the same as Dev->data
	struct request *next_rq;	// pointer to next node in the request queue
}request;*/	 		
extern Dev *device; 	 									 
extern int majorno, minorno, minors, nsectors, hardsect_size;
request_fn_proc request_func;		// as request_func_proc has been typedef'ed in blkdev.h
void transfer_func(Dev*device, unsigned long sector,unsigned long nsect, char *buffer, int write);
#endif

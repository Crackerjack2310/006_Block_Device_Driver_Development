#ifndef HEADERS_H
#define HEADERS_H
#include<linux/module.h>
#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/blkdev.h>
#include<linux/fs.h>
#include<linux/vmalloc.h>
#include<linux/slab.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sankalp Negi <sankalpnegi2310@gmail.com>");
MODULE_DESCRIPTION("Implementing gendisk structures for block device drivers");
#endif

#include"header.h"
#include"declarations.h"
#include"fops.h"
int majorno, minorno, minors, nsectors, hardsect_size;
Dev *device;				/* Our Block Device */
static int __init init_func(void)
{
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : Hello Kernel !!!\n", __func__);
	#endif 
	/*
	 * Set device params from the datasheet
	 */
	majorno = MAJORNO;			 
	minorno = MINORNO;			 
	minors = PARTITIONS;			 /* no of possible partitions of the device, for gendisk */	
	nsectors = NSECTORS; 			 /* no of sectors in the media */
	hardsect_size = HARDSECT_SIZE;	 /* This can also be used to change the default sector size of 512 bytes */
	/*
	 * Register block device  
	 * get a dynamically allocated majorno
	 * create an entry in /proc/devices
	 */
	majorno = register_blkdev(majorno, DEVNAME);								
	#ifdef DEBUG
	printk(KERN_INFO "%s : Got Major no : %d\n", __func__, majorno);	
	#endif
	if (majorno < 0)
	{
		#ifdef DEBUG
		printk(KERN_INFO "register_blkdev() failed with error code : %d\n", majorno);	
		#endif 
		goto EXIT;
	}
	majorno = ((majorno == 0) ? MAJORNO : majorno);
	/* 
	 * Make space for your Device 
	 * Clear the device structure
	 */	
	device = (Dev*)kmalloc(sizeof(Dev), GFP_KERNEL);
	if (!device)
	{	
		#ifdef DEBUG 
		printk(KERN_ERR "%s : Unable to allocate memory for device\n", __func__);
		#endif
		goto EXIT;
	}
	memset(device, 0, sizeof(Dev)); 
	/* 
	 * calculate memory required for buffer 
	 * allocate that much amount using vmalloc()
	 * clear the buffer structure
	 */
	device->size = nsectors * hardsect_size;	
	device->data = vmalloc(device->size);		// where is this memory ??
	if (!device->data)
	{
		#ifdef DEBUG 
		printk(KERN_ERR "%s : Unable to allocate memory for buffer\n", __func__);
		#endif
		goto EXIT;
	}
	#ifdef DEBUG
	printk(KERN_INFO "%s : Successfully allocated buffer of %d bytes\n", __func__, device->size);	
	#endif     			  
	memset(device->data, 0, device->size);
	/* 
	 * Get that Spinlock. 
	 */ 
	spin_lock_init(&device->lock);
	/* 
	 * Initialize the request queue, get a pointer to the first node of the request queue linked list
	 */
	device->queue = blk_init_queue(request_func, &device->lock);
	if (!device->queue)
	{
		#ifdef DEBUG
		printk(KERN_ERR "%s : Unable to initialize block device init queue\n", __func__);
		#endif		
		/*
		 * de-allocate the memory in case of an unexpected exit
		 */
		vfree(device->data);
		device->data = NULL;
		#ifdef DEBUG
		printk(KERN_ERR "%s : Freed up the device->data buffer\n", __func__);
		#endif     			  
		kfree(device);
		device = NULL;
		#ifdef DEBUG
		printk(KERN_ERR "%s : Freed up the device buffer\n", __func__);
		#endif     			  
		goto EXIT;
	}
	/* 
	 * Set logical block size
	 */
	#ifdef DEBUG
	printk(KERN_INFO "%s : Successfully Initialized block device queue\n", __func__);
	#endif     			  
	/*  
	 * Next, set a logical block size for the device, to tell 
	 * the kernel to use 'hardsect_size' instead of traditional 512 byte value
	 * earlier >> "blk_queue_hardsect_size(dev->queue, hardsect_size)"
	 * time to preserve our request in a self referencial way
	 * queuedata is of type void *
	 * queue is the address of the first node of the request_queue linked list
	 */        
	blk_queue_logical_block_size(device->queue, hardsect_size);
	device->queue->queuedata = device;								 
	/*
	 * Enter Gendisk !!
	 * each new disk structure for each different partition on disk
	 * allocate space for gendisk at device->gd  
	 */
	device->gd = alloc_disk(minors);				
	if (!device->gd) 
	{		
		#ifdef DEBUG 
		printk(KERN_ERR "%s : Unable to allocate memory for the gendisk structure\n", __func__);
		#endif
		/*
	         * clean up the request queue
        	 */ 
		blk_cleanup_queue(device->queue);
	        #ifdef DEBUG
	        printk(KERN_INFO "%s : Cleaned up the device request queue\n", __func__);
	        #endif
	        /*
	         * free up device->data buffer allocated earlier using vmalloc()
	         */
	        vfree(device->data);
	        device->data = NULL;
	        #ifdef DEBUG
	        printk(KERN_INFO "%s : Freed up device->data buffer\n", __func__);
	        #endif
	        /*
	         * free up device buffer
	         */
	        kfree(device);
	        device = NULL;
		#ifdef DEBUG
	       	printk(KERN_INFO "%s : Freed up device buffer\n", __func__);
	       	#endif
		goto EXIT;
	}
	#ifdef DEBUG
	printk(KERN_INFO "%s : Successfully allocated gendisk structure\n", __func__);
	#endif
	/*
	 * initialize some mandatory parameters
	 * else, failures will occur
	 */ 
	device->gd->major = majorno;
	device->gd->first_minor = minorno;
	device->gd->minors = minors;
	device->gd->queue = device->queue;
	device->gd->private_data = device;
	device->gd->fops = &fops;								 
	snprintf(device->gd->disk_name, DISK_NAME_LEN, "%s", DEVNAME);
	/* 
	 * resolve this !! 
	 * use -> "set_capacity(device->gd, nsectors * (hardsect_size / KERNEL_SECTOR_SIZE))"
	 * when using hardsect_size other than 512 byte, where KERNEL_SECTOR_SIZE is locally declared
	 */
	set_capacity(device->gd, nsectors);
	/*
	 * make disk available to the system
	 * by connecting it to the device table using add_disk()
	 */
	#ifdef DEBUG											 
	printk(KERN_INFO "%s : About to call add_disk()\n", __func__);
	#endif
	add_disk(device->gd); 
	#ifdef DEBUG											 
	printk(KERN_INFO "%s : Successfully added gendisk structure\n", __func__);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif 
	return 0;
EXIT:		
	#ifdef DEBUG
	printk("Block device failed due to some errors !!!\n");
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : End", __func__);
	#endif
	return 1;
}			
module_init(init_func);

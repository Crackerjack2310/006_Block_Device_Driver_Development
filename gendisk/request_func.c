#include"header.h"
#include"declarations.h"
void request_func(struct request_queue *q)
{
	struct request *request;
	int noreq = 0;
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif
	while(1)
	{
		request = blk_fetch_request(q);
		if(request)
		{
			noreq++; 
			#ifdef DEBUG
			printk(KERN_INFO "%s : request number --->> %d\n", __func__, noreq);
			#endif
			transfer_func(NULL,0, 0, 0, 0);
		}
		else
		{
			#ifdef DEBUG
			printk(KERN_INFO "%s : no more requests pending !!!\n", __func__);
			#endif
			break;
		}
	}
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
}

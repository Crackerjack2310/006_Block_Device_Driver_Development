#include"header.h"
#include"declarations.h"
static void cleanup_func(void)
{
	#ifdef DEBUG
	printk(KERN_INFO "%s : Begin\n", __func__);
	#endif 
	/*
	 * free up the gendisk structure
	 */
	del_gendisk(device->gd);
        #ifdef DEBUG 
        printk(KERN_INFO "%s : Freed up allocated memory for the gendisk structure\n", __func__);
	#endif 
	/*
	 * clean up the request queue
	 */
	//blk_cleanup_queue(device->queue);
	blk_put_queue(device->queue);
	#ifdef DEBUG
	printk(KERN_INFO "%s : Cleaned up the device request queue\n", __func__);
	#endif
	/*
	 * free up device->data buffer allocated earlier using vmalloc()
	 */
	vfree(device->data);
	device->data = NULL;
	#ifdef DEBUG
	printk(KERN_INFO "%s : Freed up device->data buffer\n", __func__);
	#endif
	/*
	 * free up device buffer
	 */
	kfree(device);
	device = NULL;
	#ifdef DEBUG
	printk(KERN_INFO "%s : Freed up device buffer\n", __func__);
	#endif
	#ifdef DEBUG
	printk(KERN_INFO "%s : Bye kernel...See u again !!!\n", __func__);
	#endif
	/*
	 * unregister the block device
	 */
	unregister_blkdev(majorno, DEVNAME);
	#ifdef DEBUG
	printk(KERN_INFO "%s : End\n", __func__);
	#endif
}
module_exit(cleanup_func);

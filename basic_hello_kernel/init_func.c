#include"header.h"
#include"declarations.h"
#include"fops.h"			/* for file_operations data structure */

unsigned int majorno;
static int __init init_func(void)
{
	printk(KERN_INFO "Hello Kernel !!!\n");
	
	majorno = register_blkdev(0, DEVNAME);	//returns majorno directly
	if (!majorno)
		goto OUT;

	printk(KERN_INFO "Major no : %d\n", majorno);	
	return 0;
OUT:	
	printk("register_chrdev failed !!! with  : %d\n", majorno);
	return 1;
}
module_init(init_func);
